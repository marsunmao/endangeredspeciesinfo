import React from 'react'
import Helmet from 'react-helmet'
import './shared.css'
import usa from './images/USAFLAG.png';
import Tabs from 'react-bootstrap/Tabs'
import Countries from './Countries'
import Tab from 'react-bootstrap/Tab'
import Animals from './Animals'
import ProtectedAreas from './ProtectedAreas'




/*
Database info needs to get here somehow, not sure how that works
Once we have database info, to build the grid you need to get info 3 instances at a time and map them to AnimalRows
props.location.state


  <div>
        <Helmet>
            <title>Countries</title>
        </Helmet>
        <ul class="nav nav-tabs">
  <li class="active"><a href="#">Home</a></li>
  <li><a href="#">Menu 1</a></li>
  <li><a href="#">Menu 2</a></li>
  <li><a href="#">Menu 3</a></li>
</ul>
        </div>
*/

class globalSearch extends React.Component {
 
  


  render() {
    

      return (
    <div>
        <header class="header">
        <Helmet>
            <title>Countries</title>
        </Helmet>        
        <Tabs defaultActiveKey="home" transition={false} id="noanim-tab-example">
<Tab eventKey="Animals" title="Animals">
            <Animals globalSearch = {this.props.location.state}/>
            </Tab>
            <Tab eventKey="Countries" title="Countries">
                <Countries globalSearch = {this.props.location.state}/>
            </Tab>
           
            <Tab eventKey="Protected Areas" title="Protected Areas">
            <ProtectedAreas globalSearch = {this.props.location.state}/>
            </Tab>
            </Tabs>
        </header>
        </div>

        )  
    };

}

export default globalSearch

