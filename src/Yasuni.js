import React from 'react'
import './shared.css'
import yasuni from './images/yasuni.jpg'
import yasuniMap from './images/yasuniMap.png'
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'

const Yasuni = () => (
<header class="header">
    <h1>Yasuni National Park</h1>
    <div class="row">
        <div class="col">
            <img src={yasuni} class="resize" alt="Yasuni"/>
        </div>
        <div class="col">
            <img src={yasuniMap} class="resize" alt="Yasuni map"/>
        </div>
    </div>
    <p>Latitude: 1°5'S </p>
    <p>Longitude: 75°55'W</p>
    <p>Country: Ecuador</p>
    <p>Area: 3,793 sq mi</p>
    <p>Established: July 26, 1979</p>
    <Link to='/protectedareas'>
        <img src={backIcon} class="link-icon" alt="Go back to Protected Areas page"/>
    </Link>
</header>
)

export default Yasuni