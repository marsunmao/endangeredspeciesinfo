import React, { useState, useEffect } from "react";
import { geoEqualEarth, geoPath } from "d3-geo";
import { feature } from "topojson-client";

const cities = [];
var destinations = {};
var content = [];
let url = "https://www.airdbd.net/api/flights/"
fetch (url)
    Promise.all([
        fetch(url),
        fetch("https://www.airdbd.net/api/flights/?limit=100&offset=100"),
        fetch("https://www.airdbd.net/api/flights/?limit=100&offset=200"),
        fetch("https://www.airdbd.net/api/flights/?limit=100&offset=300"),
        fetch("https://www.airdbd.net/api/flights/?limit=100&offset=400"),
    ])
    .then(([res, res2, res3, res4, res5]) => {
        return Promise.all([ res.json(), res2.json(), res3.json(), res4.json(), res5.json()]);
    })
    .then(([ resData, resData2, resData3, resData4, resData5]) => {
        content = resData.results.concat(resData2.results, resData3.results, resData4.results, resData5.results);
        console.log("printing content");
        console.log(content);
        for(var i = 0; i < content.length; i++) {
            try {
                var flight = content[i];
                var dest_info = flight.dst;
                var lat = dest_info.position.latitude;
                var long = dest_info.position.longitude;
                var dest_name = dest_info.city_name;
                var coords = [long, lat];
                if (dest_name in destinations) {
                    destinations[dest_name] = (destinations[dest_name] + 1);
                }
                else {
                    destinations[dest_name] = 1;
                }
                var new_res = {name:dest_name, coordinates:coords, frequency:destinations[dest_name]}
                cities.push(new_res);
            }
            catch (err) {
            }
        }
        console.log("printing cities");
        console.log(cities);
    });

const projection = geoEqualEarth()
  .scale(160)
  .translate([800 / 2, 450 / 2]);

const OtherVis3 = () => {
  const [geographies, setGeographies] = useState([]);

  useEffect(() => {
    fetch(
      "https://raw.githubusercontent.com/zimrick/react-svg-maps-tutorial/master/public/world-110m.json"
    ).then(response => {
      if (response.status !== 200) {
        console.log(`error: ${response.status}`);
        return;
      }
      response.json().then(worlddata => {
        setGeographies(
          feature(worlddata, worlddata.objects.countries).features
        );
      });
    });
  }, []);

  const handleCountryClick = countryIndex => {
    console.log("Clicked on country: ", geographies[countryIndex]);
  };

  const handleMarkerClick = i => {
    console.log("Marker: ", cities[i]);
  };

  return (
    <header className='header'>
        <h1>Density of Destination Flights</h1>
        <svg width={800} height={450} viewBox="0 0 800 450">
      <g className="countries">
        {geographies.map((d, i) => (
          <path
            key={`path-${i}`}
            d={geoPath().projection(projection)(d)}
            className="country"
            fill={`rgba(178,178,178)`}
            stroke="#000000"
            strokeWidth={0.5}
            onClick={() => handleCountryClick(i)}
          />
        ))}
      </g>
      <g className="markers">
        {cities.map((city, i) => (
          <circle
            key={`marker-${i}`}
            cx={projection(city.coordinates)[0]}
            cy={projection(city.coordinates)[1]}
            r={city.frequency / 3 + "px"}
            fill="#E91E63"
            stroke="#FFFFFF"
            className="marker"
            onClick={() => handleMarkerClick(i)}
          />
        ))}
      </g>
    </svg>
    <p>This graph depicts flight destinations along with their density of flights.</p>
    </header>
    
  );
};

export default OtherVis3;
