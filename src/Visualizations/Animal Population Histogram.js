import * as d3 from 'd3'
var dataset = [2, 14, 24, 28];

var xAxis = ["Increasing", "Decreasing", "Stable", "Unknown"]


var svgWidth = 300, svgHeight = 200, barPadding = 0;
var barWidth = (svgWidth / dataset.length);


var svg = d3.select('svg')
    .attr("width", svgWidth + 300)
    .attr("height", svgHeight + 100);
    
var barChart = svg.selectAll("bar")
    .data(dataset)
    .enter()
    .append("rect")
    .attr("class", "bar")

    .attr("y", function(d) {
         return svgHeight - d 
    })
    .attr("height", function(d) { 
        return d; 
    })
    .attr("width", barWidth - barPadding)
    .attr("transform", function (d, i) {
        var translate = [barWidth * i + 60,0]; 
        return "translate("+ translate +")";
    });

 
    
var text = svg.selectAll("text")
    .data(dataset)
    .enter()
    .append("text")
    .text(function(d) {
        return d;
    })
    .attr("y", function(d, i) {
        return svgHeight - d - 2;
    })
    .attr("x", function(d, i) {
        return barWidth * i+70;
    })
    .attr("fill");
    svg.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", svgWidth )
    .attr("y", svgHeight + 60)
    .text("Endangerment of Species");


    svg.append("text")
    .attr("class", "y label")
    .attr("text-anchor", "end")
    .attr("y", 6)
    .attr("x", -90)
    .attr("dy", ".75em")
    .attr("transform", "rotate(-90)")
    .text("Number of Species");
   

    var yscale = d3.scaleLinear()
    .domain([80, 0])
    .range([0, 100]);

    var x = d3.scaleBand()
    .domain(["Increasing", "Decreasing", "Stable", "Unknown"])
    .rangeRound([200, 500])
    .padding(0.1);

    var xAxisTranslate = 299;


var xAxis = d3.axisBottom()
    .scale(x)

    var y_axis = d3.axisLeft()
        .scale(yscale);

        svg.append("g")
        .attr("transform", "translate(60, 100)")
        .call(y_axis
        .ticks(3))


        svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(-140, " + 199  +")")
        .call(xAxis)

    // Add scales to axis
    