import * as d3 from 'd3'
import React, { Component } from 'react'
class countriesHistogram extends Component {
    componentDidMount() {
        this.drawChart();
      }
      drawChart() {

var dataset = [13, 14, 20, 5, 12, 5];

var svgWidth = 650, svgHeight = 300, barPadding = 0;
var barWidth = (svgWidth / dataset.length);


var svg = d3.select('svg')
    .attr("width", svgWidth + 300)
    .attr("height", svgHeight + 100);
    
var barChart = svg.selectAll("bar")
    .data(dataset)
    .enter()
    .append("rect")
    .attr("class", "bar")
    
    .attr("fill", "steelblue")
    .attr("y", function(d) {
         return svgHeight - d*6.7
    })
    .attr("height", function(d) { 
        return d * 6.7
    })
    .attr("width", barWidth - barPadding)
    .attr("transform", function (d, i) {
        var translate = [barWidth * i + 60,0]; 
        return "translate("+ translate +")";
    });

 
    
var text = svg.selectAll("text")
    .data(dataset)
    .enter()
    .append("text")
    .text(function(d) {
        return d;
    })
    .attr("y", function(d, i) {
        return svgHeight - (d*7);
    })
    .attr("x", function(d, i) {
        return barWidth * i+110;
    })
    .attr("fill", "white");
    svg.append("text")
    .attr("fill", "white")
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", svgWidth - 220)
    .attr("y", svgHeight + 60)
    .text("AQI (Air Quality Index)")


    svg.append("text")
    .attr("class", "y label")
    .attr("text-anchor", "end")
    .attr("y", 6)
    .attr("x", -90)
    .attr("dy", ".75em")
    .attr("fill", "white")

    .attr("transform", "rotate(-90)")
    .text("Number of Countries");
    var scale = d3.scaleLinear()
                  .domain([0, 60])
                  .range([0, 649]);

    var yscale = d3.scaleLinear()
    .domain([30, 0])
    .range([50, 250]);

    var y_axis = d3.axisLeft()
        .scale(yscale);

        var axis = d3.axisBottom(scale)
        .tickFormat(function(d, i, n) {
          return n[i + 1] ? d : d +"+";
        })
    // Add scales to axis
    
                
    var xAxisTranslate = 299;

    svg.append("g")
    .attr("transform", "translate(60, 49)")
    .call(y_axis
    .ticks(5))

    //Append group and insert axis
    svg.append("g")
    .attr("transform", "translate(60, " + xAxisTranslate  +")")
       .call(axis
        .ticks(6))
       }
       render() {
        return (
            <header class="header">
              <svg>
                <g ref='graph' />
              </svg>
              <p>This histogram is displaying the relationship between a country and the average air quality present throughout each area. </p>
              <p>This detail about country is important to understand, as it directly attributes to the healthiness of animals present in the area</p>
              <p>By analyzing relationships, there is a clear negative correlation between AQI and animal endangerment in countries, displaying</p>
              <p>how air quality plays a big role in the sustainability of animals</p>
              </header>
            );
      }
    }
    export default countriesHistogram