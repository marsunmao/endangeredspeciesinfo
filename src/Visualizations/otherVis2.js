import * as d3 from 'd3'
import React, { Component } from 'react'
import './otherVis2.css'
class otherVis2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isLoaded: false,
          result: {},
          content: [],
        };
    };

    componentDidMount() {
        var noise_count = {1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0}; 
        let url = "https://www.airdbd.net/api/cities/"
        fetch (url)
            Promise.all([
                fetch(url),
                fetch("https://www.airdbd.net/api/cities/?limit=100&offset=100"),
                fetch("https://www.airdbd.net/api/cities/?limit=100&offset=200"),
                fetch("https://www.airdbd.net/api/cities/?limit=100&offset=300"),
                fetch("https://www.airdbd.net/api/cities/?limit=100&offset=400"),

            ])
            .then(([res, res2, res3, res4, res5]) => {
               return Promise.all([ res.json(), res2.json(), res3.json(), res4.json(), res5.json()])
            })
            .then(([ resData, resData2, resData3, resData4, resData5]) => {
                this.setState({
                    isLoaded: true,
                    result: resData,
                    content: resData.results.concat(resData2.results, resData3.results, resData4.results, resData5.results),
                })
                //console.log(this.state.result.results);
                // items is the results array?? inch
                //console.log("PRINTING CONTENT");
                //console.log((this.state.content));
                //var response = this.state.result;
                //console.log("printing this.state.result");
                //console.log(response);
                //console.log("printing response.results");
                //console.log(response.results);
                    //console.log(items);
                    for(var i = 0; i < this.state.content.length; i++) {
                        var city = this.state.content[i];
                        for(var j = 1; j < 9; j++) {
                            if (city.score <= j) {
                                noise_count[j] = noise_count[j] + 1;
                                break;
                            }
                        }
                    }
                    //console.log("printing new response")
                    //console.log(response)
                
                console.log("noise_count");
                console.log(noise_count);

                this.drawChart (noise_count);
            });
        //this.getData()
        //this.drawChart (loc_countries, airports_count);
    }

    drawChart(noise_count) {

        var width = 450
        var height = 450
        var margin = 40

        // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
        var radius = Math.min(width, height) / 2 - margin

        // append the svg object to the div called 'my_dataviz'
        var svg = d3.select("#my_dataviz")
        .append("svg")
            .attr("width", width)
            .attr("height", height)
        .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        // Create dummy data
        var data = noise_count

        // set the color scale
        var color = d3.scaleOrdinal()
        .domain(data)
        .range(["#eb4034", "#eb9334", "#b7eb34", "#34ebae", "#8f34eb", "#eb34a5","#3734eb",  "#ffff"])

        // Compute the position of each group on the pie:
        var pie = d3.pie()
        .value(function(d) {return d.value; })
        var data_ready = pie(d3.entries(data))
        // Now I know that group A goes from 0 degrees to x degrees and so on.

        // shape helper to build arcs:
        var arcGenerator = d3.arc()
        .innerRadius(0)
        .outerRadius(radius)

        // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
        svg
        .selectAll('mySlices')
        .data(data_ready)
        .enter()
        .append('path')
            .attr('d', arcGenerator)
            .attr('fill', function(d){ return(color(d.data.key)) })
            .attr("stroke", "black")
            .style("stroke-width", "2px")
            .style("opacity", 0.7)

        // Now add the annotation. Use the centroid method to get the best coordinates
        svg
        .selectAll('mySlices')
        .data(data_ready)
        .enter()
        .append('text')
        .text(function(d){ return  d.data.value + ""})
        .attr("transform", function(d) { return "translate(" + (arcGenerator.centroid(d)) + ")";  })
        .style("text-anchor", "middle")
        .style("font-size", 17)
    }
        
    render() {
        if (!this.state.isLoaded){
            return <header class = "header" > <div> Loading....</div></header>
        }
        else { 
        return (
            <header className='header'>
                <h1>Number of Cities with Given Noise Scores</h1>
                <div id="my_dataviz"></div>
                <div class='my-legend'>
                <div class='legend-title'>Noise Score Legend</div>
                <div class='legend-scale'>
                <ul class='legend-labels'>
                    <li><span style={{background:"#eb4034"}}></span>0 - 1</li>
                    <li><span style={{background:"#eb9334"}}></span>1 - 2</li>
                    <li><span style={{background:"#b7eb34"}}></span>2 - 3</li>
                    <li><span style={{background:"#34ebae"}}></span>3 - 4</li>
                    <li><span style={{background:"#8f34eb"}}></span>4 - 5</li>
                    <li><span style={{background:"#eb34a5"}}></span>5 - 6</li>
                    <li><span style={{background:"#3734eb"}}></span>6 - 7</li>
                    <li><span style={{background:"#ffff"}}></span>7 - 8</li>
                </ul>
                </div>
                <div class='legend-source'> <a href="#link to source"></a></div>
                </div>
                
                <p>This graph depicts the quantities of each noise score.</p>
            </header>
            );
        }
        }
    }

    export default otherVis2