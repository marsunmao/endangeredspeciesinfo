import * as d3 from 'd3'
import React, { Component } from 'react'
class otherVis1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
          countries: [],
          count: [],
          items: [],
          isLoaded: false,
        };
    };

    componentDidMount() {
        var loc_countries = []
        var airports_count = []
        var airport_names = {}
        let url = "https://www.airdbd.net/api/airports/"
        fetch (url)
            Promise.all([
                fetch(url),
                fetch("https://www.airdbd.net/api/airports/?limit=100&offset=100"),
                fetch("https://www.airdbd.net/api/airports/?limit=100&offset=200"),
                fetch("https://www.airdbd.net/api/airports/?limit=100&offset=300"),
                fetch("https://www.airdbd.net/api/airports/?limit=100&offset=400"),
            ])
            .then(([res, res2, res3, res4, res5]) => {
               return Promise.all([ res.json(), res2.json(), res3.json(), res4.json(), res5.json()])
            })
            .then(([ resData, resData2, resData3, resData4, resData5]) => {
                this.setState({
                    isLoaded: true,
                    items: resData.results.concat(resData2.results, resData3.results, resData4.results, resData5.results),
                })
            
                console.log(this.state.items);
                for(var i = 0; i < this.state.items.length; i++) {
                    var airport = this.state.items[i];
                    console.log("airport obj: " + airport);
                    var name = airport.airport_name;
                    var country = airport.country;
                    if(country.length > 1) {
                        if(!(name in airport_names)) {
                            airport_names[name] = 1;
                            if (loc_countries.indexOf(country) != -1) {
                                var ind = loc_countries.indexOf(country);
                                airports_count[ind] = airports_count[ind] + 1;
                            }
                            else {
                                loc_countries.push(country);
                                airports_count.push(1);
                            }
                        }
                        
                    }
                    
                }
                console.log("countries found: " + loc_countries);
                console.log("count: " + airports_count);

                this.setState({
                    count: airports_count,
                    countries: loc_countries,
                })
                console.log("countries found 2: " + this.state.count);
                console.log("count 2: " + this.state.countries);

                loc_countries.sort(function(a, b){  
                    return airports_count.indexOf(a) - airports_count.indexOf(b);
                });

                var new_count = []
                var new_countries = []

                for(var i = 0; i < airports_count.length; i++) {
                    if (airports_count[i] > 9) {
                        new_count.push(airports_count[i]);
                        new_countries.push(loc_countries[i]);
                    }
                }
                  
                console.log("new countries: " + new_countries);
                console.log("new count: " + new_count);

                this.drawChart (new_countries, new_count);
            });
        //this.getData()
        //this.drawChart (loc_countries, airports_count);
    }

    drawChart(loc_countries, airports_count) {

        var dataset = airports_count;
        dataset.pop();

        var xAxis = loc_countries;
        xAxis.pop()

        console.log("this.state.items: " + this.state.items);
        console.log("dataset: " + airports_count);
        console.log("this.state.count: " + this.state.count);
        console.log("xAxis: " + loc_countries);

        var svgWidth = 600, svgHeight = 200, barPadding = 0;
    var barWidth = (svgWidth / dataset.length);


    var svg = d3.select('svg')
        .attr("width", svgWidth + 300)
        .attr("height", svgHeight + 100);
        
    var barChart = svg.selectAll("bar")
        .data(dataset)
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("fill", "steelblue")


        .attr("y", function(d) {
            return svgHeight - d 
        })
        .attr("height", function(d) { 
            return d; 
        })
        .attr("width", barWidth - barPadding)
        .attr("transform", function (d, i) {
            var translate = [barWidth * i + 60,0]; 
            return "translate("+ translate +")";
        });

    
        
    var text = svg.selectAll("text")
        .data(dataset)
        .enter()
        .append("text")
        .text(function(d) {
            return d;
        })
        .attr("y", function(d, i) {
            return svgHeight - d - 2;
        })
        .attr("x", function(d, i) {
            return barWidth * i+90;
        })
        .attr("fill", "white");
        svg.append("text")
        .attr("class", "x label")
        .attr("text-anchor", "end")
        .attr("x", 400)
        .attr("fill", "white")

        .attr("y", svgHeight + 60)
        .text("Countries");


        svg.append("text")
        .attr("class", "y label")
        .attr("text-anchor", "end")
        .attr("y", 6)
        .attr("x", -90)
        .attr("dy", ".75em")
        .attr("fill", "white")

        .attr("transform", "rotate(-90)")
        .text("Number Airports");
    
        var scale = d3.scaleLinear()
                  .domain([0, 900])
                  .range([650, 0]);

        var yscale = d3.scaleLinear()
        .domain([0, 50])
        .range([100, 0]);

        var x = d3.scaleBand()
        .domain(xAxis)
        .rangeRound([200, 800])
        .padding(0.1);

        var xAxisTranslate = 299;


    var xAxis = d3.axisBottom(scale)
        .scale(x)

        var y_axis = d3.axisLeft()
            .scale(yscale);

            svg.append("g")
            .attr("transform", "translate(60, 100)")
            .call(y_axis
            .ticks(3))


            svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(-140, " + 199  +")")
            .call(xAxis)
    }
        
    render() {
        if (!this.state.isLoaded){
            return <header class = "header" > <div> Loading....</div></header>
        }
        else { 
        return (
            <header class="header">
                <h1>Number of Airports per Country</h1>
                <svg>
                    <g ref='graph' />
                </svg>
                <p>This graph depicts the number of airports in various countries.</p>
                
                </header>
            );
        }
        }
    }

    export default otherVis1