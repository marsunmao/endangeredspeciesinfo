import React from 'react'
import './shared.css'
import otterTwo from './images/OtterTwo.jpg';
import otterEndangered from './images/otterEndangered.png';
import otterMap from './images/otterMap.png';
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'

const Otter = () => (
<header class="header">
    <h1>Giant Otter</h1>
    <div class="row">
        <div class="col">
            <img src={otterTwo} class="resize" alt="USA flag" />
        </div>
        <div class="col">
            <img src={otterEndangered} class="resize" alt="USA map" />
        </div>
        <div class="col">
            <img src={otterMap} class="resize" alt="USA satellite image" />
        </div>
    </div>
            <p>Species: Pteronura brasiliensis</p>
            <p>Habitat: Freshwater</p>
            <p>Country: Ecuador</p>
            <p>Population Size: 5,000</p>
            <p>Conservation Status: Endangered</p>
    <Link to='/Animals'>
        <img src={backIcon} class="link-icon" alt="Go back to Countries page"/>
    </Link>
</header>
)

export default Otter