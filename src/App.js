import React from 'react';
import Header from './header'
import Main from './main'
import About from './About'
import './App.css';

const openWeatherKey = "3055d070020c0c2e5b415f05fa63ae21";
var redListKey = "211f63a14d4f7d30bc939fbd44f436226d2236f61cd9105bbabc42986aa5681a";
const airQualityKey = "a455ac52659b9227aa1a3e83e8b61eb85a649f10";
const protectedAreasKey = "00afcc6068ad3f8dbde851f0e54720d3";

/*
        gitlab api need:
                author
                issues (closed by, opened by?)
                username?
                pic? (or external?)
                unit tests?
        Need to get gitlab auth/keys
*/

//gitlab personal token, expires 10/2/2020
const gitlabToken = "rdGQG-oywmAUSmgwV6os";
const gitlabTokenString = "\"PRIVATE-TOKEN: "+gitlabToken+"\" ";

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        isLoaded: false,
        countries: [],
        austinAqi: [],
        protectedAreas: [],
        endangeredCountUS: 0,
        //adding stuff for gitlab api
        gitLoaded: false,
        commits: [],
		dumb2:[],
		dumb3:[],
        issues: [],
		commitCount: [],
		issueCount: [],
		animals:[]
		
    };
  }

  componentDidMount() {
    Promise.all([
        fetch('https://api.waqi.info/search/?keyword=austin&token=' + airQualityKey),
		fetch("https://gitlab.com/api/v4/projects/14524565/repository/commits?per_page=100&page=1&private_token=" + gitlabToken),
		fetch("https://gitlab.com/api/v4/projects/14524565/repository/commits?per_page=100&page=2&private_token=" + gitlabToken),
		fetch("https://gitlab.com/api/v4/projects/14524565/issues?per_page=100&private_token=" + gitlabToken),

    ])
    .then(([ res2, commitInfo,test2, issueInfo]) => {

       return Promise.all([ res2.json(),commitInfo.json(),test2.json(), issueInfo.json()])
    })
    .then(([ data2,  commitData,test2, issueData]) => {
        this.setState({
            isLoaded: true,
            austinAqi: data2.data,
            commits: commitData,
			dumb2:test2,
            issues: issueData,
			commitCount: [],
			issueCount: [],
        });
		
		// console logs should return a list of objects, AKA a list of their respective elements
        //console.log(this.state.commits);
		//console.log(this.state.dumb2);
		//console.log(this.state.dumb3);
		console.log(this.state.animals);
		this.state.commits.push.apply(this.state.commits,this.state.dumb2);
		//commitCount = new Map([["Nikhil Bodicharla",0],["Adriana Ixba",0],["Marcus Mao",0],["David Lai",0],["Jack Su",0]]);
		//issueCount = new Map([["Nikhil Bodicharla",0],["Adriana Ixba",0],["Marcus Mao",0],["David Lai",0],["Jack Su",0]]);
		this.commitCount = [0,0,0,0,0];
		this.issueCount = [0,0,0,0,0];

		for(var i = 0; i < this.state.commits.length; i++)
		{
			var temp = this.state.commits[i].author_name;
			var email = this.state.commits[i].author_email;
			//pardon the crappy if statements, want to avoid string errors as much as possible
			if(temp === "Nikhil Bodicharla")
			{
				this.commitCount[0]+=1;
			}
			if(temp === "adrianaixba")
			{
				this.commitCount[1]+=1;
			}
			if(temp === "maomarcus")
			{
				this.commitCount[2]+=1;
			}
			if(email === "david.lai.255@utexas.edu")
			{
				this.commitCount[3]+=1;
			}
			if(temp === "Jack Su")
			{
				this.commitCount[4]+=1;
			}
			//console.log(temp);
		}
		
		console.log(this.state.issues);
		for(var i = 0; i < this.state.issues.length; i++)
		{
			
			var temp = this.state.issues[i].author.name;
			if(temp === "Nikhil Bodicharla")
			{
				this.issueCount[0]+=1;
			}
			if(temp === "Adriana Ixba")
			{
				this.issueCount[1]+=1;
			}
			if(temp === "Marcus Mao")
			{
				this.issueCount[2]+=1;
			}
			if(temp === "DavidL")
			{
				this.issueCount[3]+=1;
			}
			if(temp === "Jack Su")
			{
				this.issueCount[4]+=1;
			}
		}
		console.log(this.issueCount);
		console.log(this.commitCount);
		console.log("Nikhil Bodicharla"+" Commits: "+this.commitCount[0]+" Issues: "+this.issueCount[0]);
		console.log("Adriana Ixba"+" Commits: "+this.commitCount[1]+" Issues: "+this.issueCount[1]);
		console.log("Marcus Mao"+" Commits: "+this.commitCount[2]+" Issues: "+this.issueCount[2]);
		console.log("David Lai"+" Commits: "+this.commitCount[3]+" Issues: "+this.issueCount[3]);
		console.log("Jack Su"+" Commits: "+this.commitCount[4]+" Issues: "+this.issueCount[4]);
        this.setState({
            isLoaded: true,
            austinAqi: data2.data,
            commits: commitData,
            issues: issueData,
			commitCount: this.commitCount,
			issueCount: this.issueCount
        });
    });
	
	
}


  render() {

    /*var { isLoaded, countries, austinAqi, endangeredCountUS } = this.state;
    if (countries.length == 0) {
        return <div>Loading...</div>;
    } else {
        return (
          <ul>
            {austinAqi.map(station => (
              <li key={country.isocode}>
                {country.isocode} - {country.country}
              </li>
              <li key={station.uid}>
                {station.station.name} - {station.aqi}
              </li>
            ))}
          </ul>
          <div>Number of endangered species in the US: {endangeredCountUS}</div>
        );
      }*/
     //console.log(this.state.commitCount)
     //console.log(this.state.issueCount)
     return (
      <div>
        <Header />
        <Main commitCount={this.state.commitCount} issueCount={this.state.issueCount}/>
      </div>
    );
  }
}

export default App;