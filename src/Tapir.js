import React from 'react'
import './shared.css'
import tapirTwo from './images/tapirTwo.jpg';
import tapirEndangered from './images/tapirEndangered.png';
import tapirMap from './images/tapirMap.png';
import { Link } from 'react-router-dom'
import backIcon from './images/backIcon.png'

const Otter = () => (
<header class="header">
    <h1>Malaysian Tapir</h1>
    <div class="row">
        <div class="col">
            <img src={tapirTwo} class="resize" alt="USA flag" />
        </div>
        <div class="col">
            <img src={tapirEndangered} class="resize" alt="USA map" />
        </div>
        <div class="col">
            <img src={tapirMap} class="resize" alt="USA satellite image" />
        </div>
    </div>
            <p>Malaysian Tapir</p>
            <p>Species: Tapirus indicus</p>
            <p>Habitat: Rainforest</p>
            <p>Country: Malaysia</p>
            <p>Population Size: 2,500</p>
            <p>Conservation Status: Endangered</p>
    <Link to='/Animals'>
        <img src={backIcon} class="link-icon" alt="Go back to Countries page"/>
    </Link>
</header>
)

export default Otter