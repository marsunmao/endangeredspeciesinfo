import React from 'react'
import { Link } from 'react-router-dom'
import GlobalSearch from './globalSearch'
import { Router, Route } from 'react-router';



// The Header creates links that can be used to navigate
// between routes.
class Header extends React.Component {
   constructor(props) {
        super(props);
        this.state = {
          searchField: ''
        };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);


      };

    handleChange(event) {
      this.setState({searchField: event.target.value});
    }
    handleSubmit(event) {
    }
    render(){
      return(
  <div className="allign">
    <nav class="navbar navbar-expand-sm bg-light navbar-light">
      <ul class="navbar-nav">
        <li class="nav-item active"><Link class="nav-link" to='/'>Home</Link></li>
        <li class="nav-item active"><Link class="nav-link" to='/countries'>Countries</Link></li>
        <li class="nav-item active"><Link class="nav-link" to='/protectedareas'>Protected Areas</Link></li>
        <li class="nav-item active"><Link class="nav-link" to='/animals'>Animals</Link></li>
        <li class="nav-item dropdown active">
          <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
            Visualizations
          </a>
          <div class="dropdown-menu">
            <li class="dropdown-item"><Link class="nav-link" to='/AnimalVis'>Visualization 1</Link></li>
            <li class="dropdown-item"><Link class="nav-link" to='/CountryVis'>Visualization 2 </Link></li>
            <li class="dropdown-item"><Link class="nav-link" to='/ProtectedVis'>Visualization 3</Link></li>
            <li class="dropdown-item"><Link class="nav-link" to='/AirportsVis'>Visualization 4</Link></li>
            <li class="dropdown-item"><Link class="nav-link" to='/NoiseScoreVis'>Visualization 5</Link></li>
            <li class="dropdown-item"><Link class="nav-link" to='/FlightsVis'>Visualization 6</Link></li>
          </div>
        </li>
        <li class="nav-item active"><Link class="nav-link" to='/about'>About</Link></li>
        <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" value = {this.state.value} onChange={this.handleChange} aria-label="Search"/>
      <Link class="nav-link" to={{ pathname: '/search', state: { foo: this.state.searchField}}}>
      <button class="btn btn-outline-success my-2 my-sm-0"  onClick = {this.handleSubmit} type="submit">Search
      </button>
      </Link>
    </form>
        
      </ul>
    </nav>
  </div>
      )
    }
  }

export default Header
