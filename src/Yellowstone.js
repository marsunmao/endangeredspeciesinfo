import React from 'react'
import './shared.css'
import YellowStone from './images/YellowStone.jpg'
import yellowstoneMap from './images/yellowstoneMap.png'
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'

const Yellowstone = () => (
<header class="header">
    <h1>Yellowstone National Park</h1>
    <div class="row">
        <div class="col">
            <img src={YellowStone} class="resize" alt="Yellowstone"/>
        </div>
        <div class="col">
            <img src={yellowstoneMap} class="resize" alt="Yellowstone map"/>
        </div>
    </div>
    <p>Latitude: 44°36'N </p>
    <p>Longitude: 110°30'W</p>
    <p>Country: USA</p>
    <p>Area: 3,468 sq mi</p>
    <p>Established: March 1, 1872</p>
    <Link to='/protectedareas'>
        <img src={backIcon} class="link-icon" alt="Go back to Protected Areas page"/>
    </Link>
</header>
)

export default Yellowstone