import React from 'react'
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'
import Helmet from 'react-helmet'

class CountryPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          items: [],
          isLoaded: false,
          animals: [],
          animalsLoaded: false,
        };
      };
      
      componentDidMount() {
        var id = this.props.match.params.name;
        fetch(`https://endangeredanimals.me/api/country?q={"filters":[{"name":"name","op":"eq","val":"${id}"}]}`,
        {  mode: 'cors'})
          .then(res => res.json())
          .then(json => {
              this.setState({
                  isLoaded: true,
                  items: json.objects,
              })
              fetch(`https://endangeredanimals.me/api/animal?q={"filters":[{"name":"country","op":"eq","val":"${this.state.items[0].name}"}]}`,
              {  mode: 'cors'})
                .then(res => res.json())
                .then(json => {
                    this.setState({
                        animalsLoaded: true,
                        animals: json.objects,
                    })
                })
          })
      };

  render() {
    if (!this.state.isLoaded || !this.state.animalsLoaded){
        return <header class = "header" > <div> Loading....</div></header>
    }
    else{
    return (
      <header class="header">
        <Helmet>
            <title>{this.state.items[0].name}</title>
        </Helmet>
        
        <div class= "row">
          <div class="col-sm">
            <img src={this.state.items[0].flag_image} class="img-fluid"/>
          </div>
          <div class="col-sm">
            <img src={this.state.items[0].map_image} class="img-fluid"/>
          </div>
        </div>

        <h1>Name: {this.state.items[0].name}</h1>
        <p>AQI: {this.state.items[0].aqi}</p>
        <p>Latitude: {this.state.items[0].latitude}</p>
        <p>Longitude: {this.state.items[0].longitude}</p>
        <p>Region: {this.state.items[0].region}</p>
        <p>Protected Areas:</p>
        {this.state.items[0].areas.map(instance => 
          <Link to={`/area/${instance.name}`}>
          <p>{instance.name}</p>
        </Link>)}
        <p>Animals:</p>
        {this.state.animals.map(instance => 
          <Link to={`/animal/${instance.name}`}>
          <p>{instance.name}</p>
        </Link>)}
      </header>
    );
    }
  }
};




export default CountryPage;