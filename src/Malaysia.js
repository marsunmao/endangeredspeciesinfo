import React from 'react'
import './shared.css'
import MalaysiaFlag from './images/MalaysianFlag.png';
import MalaysiaMap from './images/MalaysiaMap.gif';
import MalaysiaSatellite from './images/MalaysiaSatellite.jpg';
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'

const Malaysia = () => (
<header class="header">
    <h1>Ecuador</h1>
    <div class="row">
        <div class="col">
            <img src={MalaysiaFlag} class="resize" alt="logo" />
        </div>
        <div class="col">
            <img src={MalaysiaMap} class="resize" alt="logo" />
        </div>
        <div class="col">
            <img src={MalaysiaSatellite} class="resize" alt="logo" />
        </div>
    </div>
  <p>Latitude: 4.2105° N </p>
    <p>Longitude: 101.9758° E</p>
    <p>Number of Protected Areas: 25,800 </p>
    <p>Air Quality Level: 444 </p>
    <p>Atmospheric Pressure: 759.212327 </p>
    <Link to='/countries'>
        <img src={backIcon} class="link-icon" alt="logo"/>
    </Link>
</header>
)

export default Malaysia