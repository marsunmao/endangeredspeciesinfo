import React from 'react'
import Helmet from 'react-helmet'
import './shared.css'
import blackFootedFerret from './images/blackFootedFerret.jpg';
import { Link } from 'react-router-dom'

/*
Database info needs to get here somehow, not sure how that works
Once we have database info, to build the grid you need to get info 3 instances at a time and map them to AnimalRows
*/

class Animals extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false,
      numPerPage: 10,
      totalPages: 0,
      currentPage: 1,
      search: '',
      searchTerms: [],
      filteredSearch: [],
      sortField: "none",
      direction: "asc",
      filterField: "none",
      filter: ""
    };
  };
  handleClick(page){
    this.state.currentPage=page
    this.runQuery()
}
  handleField(field){
        this.state.currentPage=1
    this.state.sortField=field
    this.runQuery()
}
  handleDirection(direction){
        this.state.currentPage=1
    this.state.direction=direction
    this.runQuery()
}
  handleFilterField(field){
        this.state.currentPage=1
    this.state.filterField=field
    this.runQuery()
}
  handleFilter(filter){
        this.state.currentPage=1
    this.state.filter=filter
    this.runQuery()
}

updateSearch(event){
    this.state.search = event.target.value.toLowerCase()
    this.runQuery()
}

handleFilter(event){
    this.state.filter=event.target.value
    this.runQuery()
}

runQuery() {
    var query = `https://endangeredanimals.me/api/animal?page=${this.state.currentPage}`
    if(this.state.search != "") {
        query += `&q={"filters":[{"or": [{"name":"name","op":"ilike","val":"%${this.state.search}%"},
        {"name":"habitat","op":"ilike","val":"%${this.state.search}%"},
        {"name":"status","op":"ilike","val":"%${this.state.search}%"},
        {"name":"population","op":"ilike","val":"%${this.state.search}%"},
        {"name":"species","op":"ilike","val":"%${this.state.search}%"}]}`
        if(this.state.filterField != "none" && this.state.filter != "") {
            query += `,{"name":"${this.state.filterField}","op":"ilike","val":"${this.state.filter}"}]`
        } else {
            query += "]"
        }
        if(this.state.sortField != "none") {
            query += `,"order_by":[{"field":"${this.state.sortField}","direction":"${this.state.direction}"}]}`
        } else {
            query += "}"
        }
    } else {
        if(this.state.filterField != "none" && this.state.filter != "") {
            query += `&q={"filters":[{"name":"${this.state.filterField}","op":"ilike","val":"${this.state.filter}"}]`
            if(this.state.sortField != "none") {
                query += `,"order_by":[{"field":"${this.state.sortField}","direction":"${this.state.direction}"}]}`
            } else {
                query += "}"
            }
        } else {
            if(this.state.sortField != "none") {
                query += `&q={"order_by":[{"field":"${this.state.sortField}","direction":"${this.state.direction}"}]}`
            }
        }
    }
        fetch(query,
        {  mode: 'cors'})
          .then(res => res.json())
          .then(json => {
              this.setState({
                  isLoaded: true,
                  items: json.objects,
                  totalPages: json.total_pages,
                  currentPage: json.page,   
              })
          })
}
  
  componentDidMount() {
    fetch('https://endangeredanimals.me/api/animal',
    {  mode: 'cors'})
      .then(res => res.json())
      .then(json => {
          this.setState({
              isLoaded: true,
              items: json.objects,
              totalPages: json.total_pages,
              currentPage: json.page,
              search: '',
              filteredSearch: this.items,
          })
      })
  };
  


  render() {
    var {isLoaded, items, numPerPage, totalPages, currentPage, filteredSearch} = this.state;
        if(this.props.globalSearch){
            this.state.search = this.props.globalSearch.foo;
            this.runQuery()
        }
     if(items.length >= 1) {
        filteredSearch = items.filter((animal) => {  if(animal.name) 
            return (animal.name.toLowerCase().indexOf(this.state.search) != -1  || 
            animal.habitat && animal.habitat.toLowerCase().indexOf(this.state.search) != -1    ||
            animal.status && animal.status.toLowerCase().indexOf(this.state.search) != -1    ||
            animal.population && animal.population.toLowerCase().indexOf(this.state.search) != -1    ||
            animal.species && animal.species.toLowerCase().indexOf(this.state.search) != -1)              
        }) 
    } else {
        filteredSearch = []
    }

      return (
        <header class="header">
        <Helmet>
            <title>Animals</title>
        </Helmet>
        <ul class="pagination">
   {currentPage > 1 ? <li onClick={this.handleClick.bind(this, currentPage-1)} class="page-item"><a class="page-link" href="#">Previous</a></li>
   : <li class="disabled page-item"><a class="page-link" href="#">Previous</a></li>
   }
  {currentPage > 1 && <li onClick = {this.handleClick.bind(this, currentPage-1)}class="page-item"><a class="page-link" href="#">{currentPage - 1 }</a></li>}
  <li class="active page-item"><a class="page-link" href="#">{currentPage}</a></li>
  {currentPage != totalPages && <li onClick ={this.handleClick.bind(this, currentPage+1)} class="page-item"><a class="page-link" href="#">{currentPage + 1}</a></li>}
 {currentPage != totalPages ?
  <li onClick ={this.handleClick.bind(this, currentPage+1)}class="page-item "><a class="page-link" href="#">Next</a></li>
  : <li class="disabled page-item"><a class="page-link" href="#">Next</a></li>
  }
</ul>

{!this.props.globalSearch ?
    <div class="md-form mt-0">
      <input class="form-control" type="text" value = {this.state.search} onChange = {this.updateSearch.bind(this)} placeholder="Search" aria-label="Search"/>
    </div> :  <div></div>
     }


           <div class= "row">
            <div class="col-sm-0.5">
            <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Sort
            </button>
            <div class="dropdown-menu">
                <a onClick={this.handleField.bind(this, "name")} class="dropdown-item" href="#">Name</a>
                <a onClick={this.handleField.bind(this, "habitat")} class="dropdown-item" href="#">Habitat</a>
                <a onClick={this.handleField.bind(this, "status")} class="dropdown-item" href="#">Status</a>
                <a onClick={this.handleField.bind(this, "population")} class="dropdown-item" href="#">Population</a>
                <a onClick={this.handleField.bind(this, "species")} class="dropdown-item" href="#">Species</a>
                <a onClick={this.handleField.bind(this, "none")} class="dropdown-item" href="#">None</a>
            </div>
            </div>
            </div>

            <div class="col-sm-1">
            <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Order
            </button>
            <div class="dropdown-menu">
                <a onClick={this.handleDirection.bind(this, "asc")} class="dropdown-item" href="#">Ascending</a>
                <a onClick={this.handleDirection.bind(this, "desc")} class="dropdown-item" href="#">Descending</a>
            </div>
            </div>
            </div>

            <div class="col-sm-0.5">
            <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Filter
            </button>
            <div class="dropdown-menu">
                <a onClick={this.handleFilterField.bind(this, "name")} class="dropdown-item" href="#">Name</a>
                <a onClick={this.handleFilterField.bind(this, "habitat")} class="dropdown-item" href="#">Habitat</a>
                <a onClick={this.handleFilterField.bind(this, "status")} class="dropdown-item" href="#">Status</a>
                <a onClick={this.handleFilterField.bind(this, "population")} class="dropdown-item" href="#">Population</a>
                <a onClick={this.handleFilterField.bind(this, "species")} class="dropdown-item" href="#">Species</a>
                <a onClick={this.handleFilterField.bind(this, "none")} class="dropdown-item" href="#">None</a>
            </div>
            </div>
            </div>

            <div class="col-sm-0.5">
                <div class="md-form mt-0">
                    <input class="form-control" type="text" value = {this.state.filter} onChange = {this.handleFilter.bind(this)} placeholder="Filter Value" aria-label="Filter Value"/>
                </div>
            </div>

            </div>

           <div class= "row">
            {filteredSearch.map(instance =>
                   <div class="col-sm-2">
                     <div class="card h-75 ml-10 mb-10">
                     <img class="card-img-top" src={instance.image_one} alt="Card image cap"/>
                     <div class="card-body">
                    {instance.name.toLowerCase().indexOf(this.state.search) != -1 && this.state.search != '' ?
                    <Link to={`/animal/${instance.name}`}>
                      <h5 class="card-title text-dark">
                          {instance.name.substring(0, instance.name.toLowerCase().indexOf(this.state.search))}
                          <mark>
                            {(instance.name.substring(instance.name.toLowerCase().indexOf(this.state.search), instance.name.toLowerCase().indexOf(this.state.search) + this.state.search.length) )}
                          </mark>
                          {instance.name.substring(instance.name.toLowerCase().indexOf(this.state.search) + this.state.search.length, instance.name.length)}
                      </h5>
                    </Link>:
                    <Link to={`/animal/${instance.name}`}>
                      <h5 class="card-title text-dark">{instance.name}</h5>
                    </Link>}
                     </div>
                    <ul class="list-group list-group-flush">
                        {instance.habitat && instance.habitat.toLowerCase().indexOf(this.state.search) != -1 && this.state.search != '' ?<li class="list-group-item text-dark">Habitat: {instance.habitat.substring(0, instance.habitat.toLowerCase().indexOf(this.state.search))}<mark>{(instance.habitat.substring(instance.habitat.toLowerCase().indexOf(this.state.search), instance.habitat.toLowerCase().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.habitat.substring(instance.habitat.toLowerCase().indexOf(this.state.search) + this.state.search.length, instance.habitat.length)}</li>:
                        <li class="list-group-item text-dark">Habitat: {instance.habitat}</li>}
                        {instance.status && instance.status.toLowerCase().indexOf(this.state.search) != -1 &&  this.state.search != '' ?<li class="list-group-item text-dark">Status: {instance.status.substring(0, instance.status.toLowerCase().indexOf(this.state.search))}<mark>{(instance.status.substring(instance.status.toLowerCase().indexOf(this.state.search), instance.status.toLowerCase().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.status.substring(instance.status.toLowerCase().indexOf(this.state.search) + this.state.search.length, instance.status.length)}</li>:
                        <li class="list-group-item text-dark">Status: {instance.status}</li>}
                        {instance.population && instance.population.toLowerCase().indexOf(this.state.search) != -1 &&  this.state.search != ''?<li class="list-group-item text-dark">Population: {instance.population.substring(0, instance.population.toLowerCase().indexOf(this.state.search))}<mark>{(instance.population.substring(instance.population.toLowerCase().indexOf(this.state.search), instance.population.toLowerCase().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.population.substring(instance.population.toLowerCase().indexOf(this.state.search) + this.state.search.length, instance.population.length)}</li>:
                        <li class="list-group-item text-dark">Population: {instance.population}</li>}
                        {instance.species && instance.species.toLowerCase().indexOf(this.state.search) != -1 &&  this.state.search != '' ?<li class="list-group-item text-dark">Species: {instance.species.substring(0, instance.species.toLowerCase().indexOf(this.state.search))}<mark>{(instance.species.substring(instance.species.toLowerCase().indexOf(this.state.search), instance.species.toLowerCase().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.species.substring(instance.species.toLowerCase().indexOf(this.state.search) + this.state.search.length, instance.species.length)}</li>:
                        <li class="list-group-item text-dark">Species: {instance.species}</li>}
                        {/* {instance.habitat.toLowerCase().indexOf(this.state.search) != -1 ?<li class="list-group-item text-dark">{instance.taxonId.substring(0, instance.taxonId.indexOf(this.state.search))}<mark>{(instance.taxonId.substring(instance.taxonId.indexOf(this.state.search), instance.taxonId.indexOf(this.state.search) + this.state.search.length))}</mark>{instance.taxonId.substring(instance.taxonId.indexOf(this.state.search) + this.state.search.length, instance.taxonId.length)}</li>:
                        <li class="list-group-item text-dark">Taxon Id = {instance.taxonId}</li>} */}
                    </ul>
                <div class="card-body">
            
                </div>
                    </div>
                    </div>
                )
            }
        </div>
        </header>
        )  
    };
}



export default Animals

