import React from 'react'
import backIcon from './images/backIcon.png'
import { Link } from 'react-router-dom'
import Helmet from 'react-helmet'

/*
An AnimalPage takes in an animalId when it is routed to, based on this id the attributes should be filled in from the database.
You could also use names directly as the animalId, just change the url
*/

class AnimalPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          items: [],
          isLoaded: false,
          areas: [],
          areasLoaded: false,
        };
      };
      
      componentDidMount() {
        var id = this.props.match.params.name;
        fetch(`https://endangeredanimals.me/api/animal?q={"filters":[{"name":"name","op":"eq","val":"${id}"}]}`,
        {  mode: 'cors'})
          .then(res => res.json())
          .then(json => {
              this.setState({
                  isLoaded: true,
                  items: json.objects,
              })
              fetch(`https://endangeredanimals.me/api/area?q={"filters":[{"name":"country","op":"eq","val":"${this.state.items[0].country}"}]}`,
              {  mode: 'cors'})
                .then(res => res.json())
                .then(json => {
                    this.setState({
                        areasLoaded: true,
                        areas: json.objects,
                    })
                })
          })
      };

  render() {
    // const { params: { animalId } } = this.props;
    if (!this.state.isLoaded || !this.state.areasLoaded){
        return <header class = "header" > <div> Loading....</div></header>
    }
    else{
    return (
      <header class="header">
        <Helmet>
            <title>{this.state.items[0].name}</title>
        </Helmet>
        
        <div class= "row">
          <div class="col-sm">
            <img src={this.state.items[0].image_one} class="img-fluid"/>
          </div>
          <div class="col-sm">
            <img src={this.state.items[0].image_two} class="img-fluid"/>
          </div>
          <div class="col-sm">
            <img src={this.state.items[0].image_three} class="img-fluid"/>
          </div>
        </div>

        <h1>Name: {this.state.items[0].name}</h1>
        <p>Habitat: {this.state.items[0].habitat}</p>
        <p>Status: {this.state.items[0].status}</p>
        <p>Population: {this.state.items[0].population}</p>
        <p>Species: {this.state.items[0].species}</p>
        <p>Country:</p>
        <Link to={`/country/${this.state.items[0].country}`}>
          <p>{this.state.items[0].country}</p>
        </Link>
        <p>Protected Areas:</p>
        {this.state.areas.map(instance => 
          <Link to={`/area/${instance.name}`}>
          <p>{instance.name}</p>
        </Link>)}
      </header>
    );
    }
  }
};




export default AnimalPage;