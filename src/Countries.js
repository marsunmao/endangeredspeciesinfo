import React from 'react'
import Helmet from 'react-helmet'
import './shared.css'
import usa from './images/USAFLAG.png';
import { Link } from 'react-router-dom'

/*
Database info needs to get here somehow, not sure how that works
Once we have database info, to build the grid you need to get info 3 instances at a time and map them to AnimalRows
*/

class Countries extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false,
      numPerPage: 10,
      totalPages: 0,
      currentPage: 1,
      search: '',
      searchTerms: [],
      filteredSearch: [],
      sortField: "none",
      direction: "asc",
      filterField: "none",
      filter: ""
    };
  };
  handleClick(page){
    this.state.currentPage=page
    this.runQuery()
}
  handleField(field){
        this.state.currentPage=1
    this.state.sortField=field
    this.runQuery()
}
  handleDirection(direction){
        this.state.currentPage=1
    this.state.direction=direction
    this.runQuery()
}
  handleFilterField(field){
        this.state.currentPage=1
    this.state.filterField=field
    this.runQuery()
}
  handleFilter(filter){
        this.state.currentPage=1
    this.state.filter=filter
    this.runQuery()
}

updateSearch(event){
    this.state.search = event.target.value.toLowerCase()
    this.runQuery()
}

handleFilter(event){
    this.state.filter=event.target.value
    this.runQuery()
}

runQuery() {
    var query = `https://endangeredanimals.me/api/country?page=${this.state.currentPage}`
    if(this.state.search != "") {
        query += `&q={"filters":[{"or": [{"name":"name","op":"ilike","val":"%${this.state.search}%"},
        {"name":"region","op":"ilike","val":"%${this.state.search}%"}]}`
        if(this.state.filterField != "none" && this.state.filter != "") {
            query += `,{"name":"${this.state.filterField}","op":"eq","val":"${this.state.filter}"}]`
        } else {
            query += "]"
        }
        if(this.state.sortField != "none") {
            query += `,"order_by":[{"field":"${this.state.sortField}","direction":"${this.state.direction}"}]}`
        } else {
            query += "}"
        }
    } else {
        if(this.state.filterField != "none" && this.state.filter != "") {
            query += `&q={"filters":[{"name":"${this.state.filterField}","op":"eq","val":"${this.state.filter}"}]`
            if(this.state.sortField != "none") {
                query += `,"order_by":[{"field":"${this.state.sortField}","direction":"${this.state.direction}"}]}`
            } else {
                query += "}"
            }
        } else {
            if(this.state.sortField != "none") {
                query += `&q={"order_by":[{"field":"${this.state.sortField}","direction":"${this.state.direction}"}]}`
            }
        }
    }
        fetch(query,
        {  mode: 'cors'})
          .then(res => res.json())
          .then(json => {
              this.setState({
                  isLoaded: true,
                  items: json.objects,
                  totalPages: json.total_pages,
                  currentPage: json.page,   
              })
          })
}
  
  componentDidMount() {
    fetch('https://endangeredanimals.me/api/country',
    {  mode: 'cors'})
      .then(res => res.json())
      .then(json => {
          this.setState({
              isLoaded: true,
              items: json.objects,
              totalPages: json.total_pages,
              currentPage: json.page,
              search: '',
              filteredSearch: this.items,
          })
      })
  };
  


  render() {
      var {isLoaded, items, numPerPage, totalPages, currentPage, filteredSearch} = this.state;
     if(this.props.globalSearch){
         this.state.search = this.props.globalSearch.foo;
         this.runQuery()
             }
    console.log(this.props.globalSearch);
    console.log(this.state.search);
     if(items.length >= 1) {
        filteredSearch = items.filter((country) => {  if(country.name) 
            return (country.name.toLowerCase().indexOf(this.state.search) != -1  || 
            country.aqi && country.aqi.toString().indexOf(this.state.search) != -1    ||
            country.latitude && country.latitude.toString().indexOf(this.state.search) != -1    ||
            country.longitude && country.longitude.toString().indexOf(this.state.search) != -1    ||
            country.region && country.region.toLowerCase().indexOf(this.state.search) != -1)              
        }) 
    } else {
        filteredSearch = []
    }

      return (
        <header class="header">
        <Helmet>
            <title>Countries</title>
        </Helmet>
        <ul class="pagination">
   {currentPage > 1 ? <li onClick={this.handleClick.bind(this, currentPage-1)} class="page-item"><a class="page-link" href="#">Previous</a></li>
   : <li class="disabled page-item"><a class="page-link" href="#">Previous</a></li>
   }
  {currentPage > 1 && <li onClick = {this.handleClick.bind(this, currentPage-1)}class="page-item"><a class="page-link" href="#">{currentPage - 1 }</a></li>}
  <li class="active page-item"><a class="page-link" href="#">{currentPage}</a></li>
  {currentPage != totalPages && <li onClick ={this.handleClick.bind(this, currentPage+1)} class="page-item"><a class="page-link" href="#">{currentPage + 1}</a></li>}
 {currentPage != totalPages ?
  <li onClick ={this.handleClick.bind(this, currentPage+1)}class="page-item "><a class="page-link" href="#">Next</a></li>
  : <li class="disabled page-item"><a class="page-link" href="#">Next</a></li>
  }
</ul>
 
{!this.props.globalSearch ?
<div class="md-form mt-0">
  <input class="form-control" type="text" value = {this.state.search} onChange = {this.updateSearch.bind(this)} placeholder="Search" aria-label="Search"/>
</div> :  <div></div>
 }


           <div class= "row">
            <div class="col-sm-0.5">
            <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Sort
            </button>
            <div class="dropdown-menu">
                <a onClick={this.handleField.bind(this, "name")} class="dropdown-item" href="#">Name</a>
                <a onClick={this.handleField.bind(this, "aqi")} class="dropdown-item" href="#">AQI</a>
                <a onClick={this.handleField.bind(this, "latitude")} class="dropdown-item" href="#">Latitude</a>
                <a onClick={this.handleField.bind(this, "longitude")} class="dropdown-item" href="#">Longitude</a>
                <a onClick={this.handleField.bind(this, "region")} class="dropdown-item" href="#">Region</a>
                <a onClick={this.handleField.bind(this, "none")} class="dropdown-item" href="#">None</a>
            </div>
            </div>
            </div>

            <div class="col-sm-1">
            <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Order
            </button>
            <div class="dropdown-menu">
                <a onClick={this.handleDirection.bind(this, "asc")} class="dropdown-item" href="#">Ascending</a>
                <a onClick={this.handleDirection.bind(this, "desc")} class="dropdown-item" href="#">Descending</a>
            </div>
            </div>
            </div>

            <div class="col-sm-0.5">
            <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Filter
            </button>
            <div class="dropdown-menu">
                <a onClick={this.handleFilterField.bind(this, "name")} class="dropdown-item" href="#">Name</a>
                <a onClick={this.handleFilterField.bind(this, "aqi")} class="dropdown-item" href="#">AQI</a>
                <a onClick={this.handleFilterField.bind(this, "latitude")} class="dropdown-item" href="#">Latitude</a>
                <a onClick={this.handleFilterField.bind(this, "longitude")} class="dropdown-item" href="#">Longitude</a>
                <a onClick={this.handleFilterField.bind(this, "region")} class="dropdown-item" href="#">Region</a>
                <a onClick={this.handleFilterField.bind(this, "none")} class="dropdown-item" href="#">None</a>
            </div>
            </div>
            </div>

            <div class="col-sm-0.5">
                <div class="md-form mt-0">
                    <input class="form-control" type="text" value = {this.state.filter} onChange = {this.handleFilter.bind(this)} placeholder="Filter Value" aria-label="Filter Value"/>
                </div>
            </div>

            </div>

           <div class= "row">
            {filteredSearch.map(instance =>
                   <div class="col-sm-2">
                     <div class="card h-75 ml-10 mb-10">
                     <img class="card-img-top" src={instance.flag_image} alt="Card image cap"/>
                     <div class="card-body">
                    {instance.name.toLowerCase().indexOf(this.state.search) != -1 && this.state.search != '' ?
                    <Link to={`/country/${instance.name}`}>
                    <h5 class="card-title text-dark">
                      {instance.name.substring(0, instance.name.toLowerCase().indexOf(this.state.search))}
                      <mark>
                        {(instance.name.substring(instance.name.toLowerCase().indexOf(this.state.search), instance.name.toLowerCase().indexOf(this.state.search) + this.state.search.length) )}
                      </mark>
                      {instance.name.substring(instance.name.toLowerCase().indexOf(this.state.search) + this.state.search.length, instance.name.length)}
                    </h5>
                    </Link>:
                    <Link to={`/country/${instance.name}`}>
                    <h5 class="card-title text-dark">{instance.name}</h5></Link>}
                     </div>
                    <ul class="list-group list-group-flush">
                        {instance.aqi && instance.aqi.toString().indexOf(this.state.search) != -1 && this.state.search != '' ?<li class="list-group-item text-dark">AQI: {instance.aqi.substring(0, instance.aqi.toString().indexOf(this.state.search))}<mark>{(instance.aqi.substring(instance.aqi.toString().indexOf(this.state.search), instance.aqi.toString().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.aqi.substring(instance.aqi.toString().indexOf(this.state.search) + this.state.search.length, instance.aqi.length)}</li>:
                        <li class="list-group-item text-dark">AQI: {instance.aqi}</li>}
                        {instance.latitude && instance.latitude.toString().indexOf(this.state.search) != -1 &&  this.state.search != '' ?<li class="list-group-item text-dark">Latitude: {instance.latitude.substring(0, instance.latitude.toString().indexOf(this.state.search))}<mark>{(instance.latitude.substring(instance.latitude.toString().indexOf(this.state.search), instance.latitude.toString().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.latitude.substring(instance.latitude.toString().indexOf(this.state.search) + this.state.search.length, instance.latitude.length)}</li>:
                        <li class="list-group-item text-dark">Latitude: {instance.latitude}</li>}
                        {instance.longitude && instance.longitude.toString().indexOf(this.state.search) != -1 &&  this.state.search != ''?<li class="list-group-item text-dark">Longitude: {instance.longitude.substring(0, instance.longitude.toString().indexOf(this.state.search))}<mark>{(instance.longitude.substring(instance.longitude.toString().indexOf(this.state.search), instance.longitude.toString().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.longitude.substring(instance.longitude.toString().indexOf(this.state.search) + this.state.search.length, instance.longitude.length)}</li>:
                        <li class="list-group-item text-dark">Longitude: {instance.longitude}</li>}
                        {instance.region && instance.region.toLowerCase().indexOf(this.state.search) != -1 &&  this.state.search != '' ?<li class="list-group-item text-dark">Region: {instance.region.substring(0, instance.region.toLowerCase().indexOf(this.state.search))}<mark>{(instance.region.substring(instance.region.toLowerCase().indexOf(this.state.search), instance.region.toLowerCase().indexOf(this.state.search) + this.state.search.length))}</mark>{instance.region.substring(instance.region.toLowerCase().indexOf(this.state.search) + this.state.search.length, instance.region.length)}</li>:
                        <li class="list-group-item text-dark">Region: {instance.region}</li>}
                        {/* {instance.aqi.toLowerCase().indexOf(this.state.search) != -1 ?<li class="list-group-item text-dark">{instance.taxonId.substring(0, instance.taxonId.indexOf(this.state.search))}<mark>{(instance.taxonId.substring(instance.taxonId.indexOf(this.state.search), instance.taxonId.indexOf(this.state.search) + this.state.search.length))}</mark>{instance.taxonId.substring(instance.taxonId.indexOf(this.state.search) + this.state.search.length, instance.taxonId.length)}</li>:
                        <li class="list-group-item text-dark">Taxon Id = {instance.taxonId}</li>} */}
                    </ul>
                <div class="card-body">
            
                </div>
                    </div>
                    </div>
                )
            }
        </div>
        </header>
        )  
    };
}



export default Countries

