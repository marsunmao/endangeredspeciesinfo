from datetime import datetime
import sqlalchemy
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker, scoped_session, relationship
from sqlalchemy.ext.declarative import declarative_base
import psycopg2
import requests
import random
import os
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://postgres:swe12345@endangeredanimals.cgapldnbafiy.us-east-2.rds.amazonaws.com:5432/endangeredAnimals"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
engine_string = "postgresql+psycopg2://postgres:swe12345@endangeredanimals.cgapldnbafiy.us-east-2.rds.amazonaws.com:5432/endangeredAnimals"
engine = create_engine(engine_string, pool_size=10, max_overflow=20)
db = SQLAlchemy(app)

Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
Base.metadata.bind = engine

migrate = Migrate()
migrate.init_app(app, db)
manager = Manager(app)

manager.add_command("db", MigrateCommand)

# for many to many relationship between animals and countries

ecology = db.Table(
    "ecology",
    db.Column("animal_id", db.Integer, db.ForeignKey("animal.animal_id")),
    db.Column("country_id", db.Integer, db.ForeignKey("country.country_id")),
)


class Animal(db.Model):
    __tablename__ = "animal"

    animal_id = db.Column(db.Integer, primary_key=True)
    taxonId = db.Column(db.Float(), unique=True, nullable=False)
    name = db.Column(db.String(120), unique=True)
    species = db.Column(db.String(120), unique=True, nullable=False)
    habitat = db.Column(db.String(120))
    country = db.Column(db.String(120))
    population = db.Column(db.String(120))
    status = db.Column(db.String(120))
    image_one = db.Column(db.String(500))
    image_two = db.Column(db.String(500))
    image_three = db.Column(db.String(500))
    # can call Country.ecologies and will return an iterable of db objects
    # for i in country1.ecologies: print(i.name)
    ecologies = db.relationship(
        "Country", secondary=ecology, backref=db.backref("ecologies", lazy="dynamic")
    )


class Area(db.Model):
    __tablename__ = "area"

    area_id = db.Column(db.Integer, primary_key=True)
    wdpa_id = db.Column(db.Integer, unique=True)
    name = db.Column(db.String(120), unique=True)
    country = db.Column(db.String(120), nullable=False)
    area = db.Column(db.Float(), primary_key=False)
    lastUpdated = db.Column(db.String(120))
    image_one = db.Column(db.String(500))
    image_two = db.Column(db.String(500))
    image_three = db.Column(db.String(500))
    country_id = db.Column(db.Integer, db.ForeignKey("country.country_id"))


class Country(db.Model):
    __tablename__ = "country"

    country_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    region = db.Column(db.String(120))
    latitude = db.Column(db.Float())
    longitude = db.Column(db.Float())
    aqi = db.Column(db.Float())
    pm10 = db.Column(db.Float())
    areasCount = db.Column(db.Integer)
    flag_image = db.Column(db.String(500))
    map_image = db.Column(db.String(500))
    # can do country1.areas and get a list of areas
    areas = db.relationship("Area", backref="location")


# Base.metadata.create_all()

if __name__ == "__main__":
    manager.run()
