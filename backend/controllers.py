from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker, scoped_session, relationship
from sqlalchemy.ext.declarative import declarative_base
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from models import Animal, Country, Area, app, Session
import flask_restless

""" to define api blueprints """
s = scoped_session(Session)
manager = flask_restless.APIManager(app, session=s)

animal_api_blueprint = manager.create_api_blueprint(Animal, methods=["GET"])
country_api_blueprint = manager.create_api_blueprint(Country, methods=["GET"])
area_api_blueprint = manager.create_api_blueprint(Area, methods=["GET"])

app.register_blueprint(animal_api_blueprint)
app.register_blueprint(country_api_blueprint)
app.register_blueprint(area_api_blueprint)
