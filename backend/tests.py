#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------
from io import StringIO
from unittest import main, TestCase
from controllers import app


class TestBackend(TestCase):
    # ----
    # read
    # ----
    def setUp(self):
        app.config["TESTING"] = True
        self.app = app.test_client()

    def test_api_server(self):
        response = self.app.get("/", follow_redirects=True)
        self.assertEqual(response.status_code, 404)

    def test_api_animal(self):
        response = self.app.get("/api/animal", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_api_animal_call(self):
        response = self.app.get("/api/animal", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)

    def test_api_animal_call2(self):
        response = self.app.get(
            '/api/animal?q={"filters}":[{"name":"name","op":"eq","val":"Gray Triggerfish"}]}',
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)

    def test_api_country(self):
        response = self.app.get("/api/country", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_api_country_call(self):
        response = self.app.get("/api/country", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)

    def test_api_country_call2(self):
        response = self.app.get(
            '/api/country?q={"filters}":[{"name":"name","op":"eq","val":"Belgium"}]}',
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)

    def test_api_area(self):
        response = self.app.get("/api/area", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_api_area2(self):
        response = self.app.get("/api/area?page=3", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_api_area_call(self):
        response = self.app.get("/api/area", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        json = response.json
        self.assertGreater(json["num_results"], 0)


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
