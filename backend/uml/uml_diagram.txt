Class Animal {
  animal_id   Integer
  taxon_id   Integer
  name   String
  species   String
  habitat   String
  country   String
  population   String
  status   String
}

Class Country {
   country_id   Integer
   name   String
   region   String
   latitude   Integer
   longitude   Integer
   aqi   Integer
   pm10   Integer
   areas_count   Integer
}

Class Area {
    id   Integer
    wdpda_id  Integer
    name   String
    country   String
    area   Integer
    last_updated  String
}

Animal -> Area: "one-to-one"
Country  ->  Animal: "many-to-many"
Country -> Area: "one-to-many"

@enduml