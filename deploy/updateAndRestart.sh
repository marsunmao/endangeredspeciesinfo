#!/bin/bash

# any future command that fails will exit the script
set -e

# kill running docker containers
#docker stop backend || true && docker rm backend || true
#docker stop front || true && docker rm front || true
#docker rm $(docker ps -a -q) || true

# move to the repo

#echo "moving to repo"

sudo su
#cd /srv/www/endangeredanimals.me/endangeredanimals
#docker-compose down
#cd ..


# Install newest repo
cd ..
docker-compose down
rm -rf endangeredspeciesinfo
git clone https://gitlab.com/marsunmao/endangeredspeciesinfo.git

# rebuild it
cd endangeredspeciesinfo

docker-compose build
docker-compose up -d